package com.trendyol.productservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class ProductMessage {
    private Integer productId;
    private String stockId;
    private NotificationType type;
    private Map<String, String> props = new HashMap<>();
}
