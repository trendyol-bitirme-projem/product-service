package com.trendyol.productservice.model;

import lombok.Getter;

@Getter
public enum NotificationType {
    PRICE_CHANGED,
    STOCK_IS_LOW,
    STOCK_IS_FINISH
}
