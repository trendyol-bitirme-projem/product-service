package com.trendyol.productservice.service;

import com.trendyol.productservice.entity.ProductEntity;
import com.trendyol.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductService {

    public final KafkaService kafkaService;
    public final ProductRepository productRepository;

    public ProductEntity saveProduct(ProductEntity productEntity) throws Exception {
        if (productEntity.getId() != null){
            throw new Exception("Use update method to update the product");
        }
        productEntity.setId(Objects.hash(UUID.randomUUID().toString()));
        validateProduct(productEntity);
        return productRepository.save(productEntity);
    }

    public ProductEntity getProduct(Integer id){
        return productRepository.findById(id).orElse(null);
    }
    public List<ProductEntity> getProducts(List<Integer> ids){
        return productRepository.findByIdIn(ids);
    }

    public ProductEntity updateProduct(ProductEntity productEntity) throws Exception {
        if (productEntity.getId() == null){
            throw new Exception("Use post method to create a product");
        }
        Optional<ProductEntity> existProductEntity = productRepository.findById(productEntity.getId());

        if (existProductEntity.isPresent()){
            validateProduct(productEntity);
            return productRepository.save(productEntity);
        }else {
            throw new Exception("Product isn't found");
        }
    }

    public void deleteProduct(Integer id){
        productRepository.deleteById(id);
    }

    private void validateProduct(ProductEntity product) throws Exception {
        if (StringUtils.isEmpty(product.getName())){
            throw new Exception("Product name can't be empty");
        }
        if (StringUtils.isEmpty(product.getCategory())){
            throw new Exception("Product category can't be empty");
        }
        if (StringUtils.isEmpty(product.getImage())){
            throw new Exception("Product image can't be empty");
        }
        if (StringUtils.isEmpty(product.getDescription())){
            throw new Exception("Product description can't be empty");
        }
        if (product.getOwnerId() == null){
            throw new Exception("Product owner can't be empty");
        }
    }
}
