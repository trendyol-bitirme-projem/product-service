package com.trendyol.productservice.service;

import com.trendyol.productservice.entity.ProductEntity;
import com.trendyol.productservice.model.NotificationType;
import com.trendyol.productservice.model.ProductMessage;
import com.trendyol.productservice.model.Stock;
import com.trendyol.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class StockService {
    public final KafkaService kafkaService;
    public final ProductService productService;
    public final ProductRepository productRepository;

    public Stock addStock(Integer productId, Stock stock) throws Exception {
        ProductEntity product = productService.getProduct(productId);
        if (product != null && StringUtils.isEmpty(stock.getId())) {
            stock.setId(UUID.randomUUID().toString());
            product.getStocks().add(stock);
            productService.updateProduct(product);
        }
        return stock;
    }

    public Stock updateStock(Integer productId, Stock stock) throws Exception {
        ProductEntity product = productService.getProduct(productId);
        if (product != null && !StringUtils.isEmpty(stock.getId())) {
            Stock existStock = product.getStocks().stream().filter(k -> k.getId().equals(stock.getId())).findFirst().orElse(null);
            if (existStock != null) {
                existStock.setColor(stock.getColor());
                existStock.setQuantity(stock.getQuantity());
                existStock.setSize(stock.getSize());
                existStock.setImage(stock.getImage());
            }
            productService.updateProduct(product);
            sendKafkaEvent(product, existStock, productId);
            return existStock;
        }
        return null;
    }

    public Stock increaseStockQuantity(Integer productId, String stockId, Integer quantity) throws Exception {
        ProductEntity product = productService.getProduct(productId);
        if (product != null && !StringUtils.isEmpty(stockId)) {
            Stock existStock = product.getStocks().stream().filter(k -> k.getId().equals(stockId)).findFirst().orElse(null);
            if (existStock != null) {
                existStock.setQuantity(existStock.getQuantity()+quantity);
            }
            productService.updateProduct(product);
            sendKafkaEvent(product, existStock, productId);
            return existStock;
        }
        return null;
    }

    public Stock decreaseStockQuantity(Integer productId, String stockId, Integer quantity) throws Exception {
        ProductEntity product = productService.getProduct(productId);
        if (product != null && !StringUtils.isEmpty(stockId)) {
            Stock existStock = product.getStocks().stream().filter(k -> k.getId().equals(stockId)).findFirst().orElse(null);
            if (existStock != null) {
                existStock.setQuantity(existStock.getQuantity()-quantity);
            }
            productService.updateProduct(product);
            sendKafkaEvent(product, existStock, productId);
            return existStock;
        }
        return null;
    }

    public void sendKafkaEvent(ProductEntity product, Stock newStock, Integer productId){
        if (newStock.getQuantity() == 0) {
            ProductMessage message = new ProductMessage();
            message.setProductId(productId);
            message.setStockId(newStock.getId());
            message.setType(NotificationType.STOCK_IS_FINISH);
            message.getProps().put("productName", product.getName());
            kafkaService.sendProductMessage(message);
        } else if (newStock.getQuantity() <= 3) {
            ProductMessage message = new ProductMessage();
            message.setProductId(productId);
            message.setStockId(newStock.getId());
            message.setType(NotificationType.STOCK_IS_LOW);
            message.getProps().put("productName", product.getName());
            kafkaService.sendProductMessage(message);
        }
    }
}
