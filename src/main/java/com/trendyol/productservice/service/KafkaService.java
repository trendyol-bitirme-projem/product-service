package com.trendyol.productservice.service;

import com.trendyol.productservice.model.ProductMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KafkaService {

    @Value("${spring.kafka.topic.product-message}")
    private String productMessageDestination;

    private final KafkaTemplate<String, ProductMessage> kafkaTemplate;

    public void sendProductMessage(ProductMessage productMessage) {
        Message<ProductMessage> kafkaMessage = MessageBuilder
                .withPayload(productMessage)
                .setHeader(KafkaHeaders.TOPIC, productMessageDestination)
                .build();
        kafkaTemplate.send(kafkaMessage);
    }

}
