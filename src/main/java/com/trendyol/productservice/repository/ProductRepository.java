package com.trendyol.productservice.repository;

import com.trendyol.productservice.entity.ProductEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProductRepository extends MongoRepository<ProductEntity, Integer> {
    List<ProductEntity> findByIdIn(List<Integer> ids);

}
