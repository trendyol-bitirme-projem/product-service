package com.trendyol.productservice.entity;

import com.trendyol.productservice.model.Stock;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Document("products")
public class ProductEntity {
    @Id
    private Integer id;
    private String name;
    private String description;
    private String image;
    private String category;
    private Map<String, String> attributes = new HashMap<>();
    private List<String> information = new ArrayList<>();
    private Integer ownerId;
    private List<Stock> stocks = new ArrayList<>();
}
