package com.trendyol.productservice.controller;

import com.trendyol.productservice.entity.ProductEntity;
import com.trendyol.productservice.model.Stock;
import com.trendyol.productservice.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/stock")
@RequiredArgsConstructor
public class StockController {
    public final StockService stockService;


    @PostMapping
    public ResponseEntity<?> addStock(@RequestBody Stock stock, @RequestParam Integer productId) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(stockService.addStock(productId, stock));
    }

    @PutMapping
    public ResponseEntity<?> updateStock(@RequestBody Stock stock, @RequestParam Integer productId) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(stockService.updateStock(productId, stock));
    }

    @PostMapping("/increase")
    public ResponseEntity<?> increaseStockQuantity(@RequestParam Integer productId,
                                                   @RequestParam String stockId,
                                                   @RequestParam Integer quantity) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(stockService.increaseStockQuantity(productId, stockId, quantity));
    }

    @PostMapping("/decrease")
    public ResponseEntity<?> decreaseStockQuantity(@RequestParam Integer productId,
                                                   @RequestParam String stockId,
                                                   @RequestParam Integer quantity) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(stockService.decreaseStockQuantity(productId, stockId, quantity));
    }
}
