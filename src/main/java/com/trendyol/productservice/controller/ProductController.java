package com.trendyol.productservice.controller;

import com.trendyol.productservice.entity.ProductEntity;
import com.trendyol.productservice.service.ProductService;
import jdk.dynalink.linker.LinkerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {
    public final ProductService productService;


    @PostMapping
    public ResponseEntity<?> saveProduct(@RequestBody ProductEntity userEntity) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(productService.saveProduct(userEntity));
    }

    @GetMapping
    public ProductEntity getProduct(@RequestParam(value = "id") Integer id){
        return productService.getProduct(id);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteProduct(@RequestParam(value = "id") Integer id) {
        productService.deleteProduct(id);
        return ResponseEntity.status(HttpStatus.OK).body("Product is removed.");
    }

    @PutMapping
    public void updateUser(@RequestBody ProductEntity productEntity) throws Exception {
        productService.updateProduct(productEntity);
    }
    @GetMapping("/list")
    public List<ProductEntity> getProducts(@RequestParam(value = "ids") List<Integer> ids){
        return productService.getProducts(ids);
    }


}
